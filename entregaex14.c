//14. Write a program that asks for a sequence of numbers to the user and show the mean of all of them. Process stops only when number 0 is introduced.

#include <stdio.h> // Introducir libreria "stdio.h"

// Punto de entrada del programa
int main()
{
    printf("Programa para calcular la media de los numeros introducidos a continuacion.\n"); // Mostrar en la pantalla la explicacion del programa
    printf("(Para obtener la media: introducir numero 0)\n\n");
    
    float tmp; // Declarar la variable decimal "tmp"
    int cont = 0; // Declarar la variable entera "cont" y declararle el valor 0
    float sumatotal = 0; // Declarar la variable decimal "sumatotal" y declararle el valor 0
    
    printf("Dame un nuemro: "); // Pedir un numero al usuario
    
    scanf("%f",&tmp); // Determinar valor introducido a "tmp"
    getchar();
    
    while ( tmp != 0 ) // Mientras la variable tmp sea diferente de 0, sumar cada numero introducido a la variable "sumatotal", añadir valor 1 a "cont" y pedir otro numero y determinarlo en "tmp"
    {
    sumatotal += tmp;
    cont++;
    
    printf("Dame otro numero: ");
    
    scanf("%f",&tmp);
    getchar();
    }
    
    printf("\nLa media total es: %f", sumatotal/=cont); // Al añadir un valor negativo a la variable "tmp" calcular la media dividiendo "sumatotal" por "tmp" <--(cantidad de numeros sumados)
    
    getchar();
    return(0); // Devolver 0. (Final del programa)
}