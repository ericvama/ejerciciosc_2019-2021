#include <stdio.h>
#include <stdlib.h>

int main()
{
    int radio1;
    
    printf("Escribe el radio de una circunferencia:");

    scanf ("%d",&radio1);
    
    getchar();
  
    printf("El diametro de la circunferencia es:%d\n",radio1*2);
    printf("La area de la circunferencia es:%5.5f\n",radio1*radio1*3.14);
    
    getchar();
    return 0;
}