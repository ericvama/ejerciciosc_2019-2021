//10. Write a program that asks for 2 angles of a triangle and find the third angle.

#include <stdio.h> // Importar libreria "stdio.h"

// Punto de entrada del programa
int main()
{
    printf("Dame el angulo1 de un triangulo:"); // Perdir un angulo del triangulo

    float angulo1; // Declarar la variable decimal "angulo1"
    
    scanf("%f",&angulo1); //Determinar valor introducido a "angulo1"
    getchar();
    
     while (( angulo1 <= 0 ) || ( angulo1 == 360 ) || ( angulo1 == 180 )) // Si se intoduce un angulo negativo, = 0, = 360 o = 180, pedir otro angulo positivo diferente de 0, 360 y 180
    {
        printf("Dame el angulo1, que sea mayor que zero y diferente de 360 y 180:");
        
        scanf("%f",&angulo1);
        getchar();
    }
    
    
    if (angulo1 > 360) // Si el angulo introducido pasa de los 360º,es decir que da al menos una vuelta entera, se le restan el numero de vueltas a "angulo1"
    {
        int angulo11 = angulo1;
        int vuelta = angulo11 /= 360;
        angulo1 -= (vuelta *= 360); 
    }
    
    printf("\nDame el angulo2 del triangulo:"); // Pedir el segundo angulo del triangulo
    
    float angulo2; // Declarar la variable decimal "angulo2"
    
    scanf("%f",&angulo2); //Determinar valor introducido a "angulo2"
    getchar();
    
     while (( angulo2 <= 0 ) || ( angulo2 == 360 ) || ( angulo2 == 180 )) // Si se intoduce un angulo negativo, = 0, = 360 o = 180, pedir otro angulo positivo diferente de 0, 360 y 180
    {
        printf("Dame el angulo2, que sea mayor que zero y diferente de 360 y 180:");
        scanf("%f",&angulo2);
        getchar();
    }
    
    if (angulo2 > 360) // Si el angulo introducido pasa de los 360º,es decir que da al menos una vuelta entera, se le restan el numero de vueltas a "angulo2"
    {
        int angulo21 = angulo2;
        int vuelta2 = angulo21 /= 360;
        angulo2 -= (vuelta2 *= 360);
    }
    
    float angulo3; // Declarar la variable decimal "angulo3"
    angulo3 = 180.f - (angulo1 + angulo2); // Encontrar el tercer angulo (sabiendo que la suma de los 3 es 180) y declararlo a "angulo3"
    
    if (angulo3 <= 0) // Si "angulo3" es menor o igual que zero, notificar que no existe este triangulo, si es mayor que 0 dar el resultado
    {
        printf("\nLo siento, este triangulo no existe, asi que voy a cerrar el programa");
    }
    else
    {
        printf("\nEl tercer angulo del trianglo es: %f", angulo3);
    }
    
    getchar();
    return(0); // Devolver 0. (Final del programa)
}