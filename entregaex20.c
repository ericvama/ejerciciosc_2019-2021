/*20. Write a calculator program. Program flow should be as follow:
1) Ask for a number (float)
2) Ask for desired operation: '+', '-', '*', '/'
3) Ask for another number (float)
4) Show operation and results
5) Go to step 2*/

#include <stdio.h> // Introducir libreria "stdio.h"

// Punto de entrada del programa
int main ()
{
    printf("Dame un numero: "); // Pedir al usuario un numero
    
    float num1; // Declarar la variable decimal "num1"
    
    scanf ("%f",&num1); // Determinar el valor introducido a la variable "num1"
    getchar();
    
    printf("\nQue operacion quieres hacer? (Sumar = +, Restar = -, Multiplicar = *, Dividir = /): "); // Pedir al usuario la operacion que quiere hacer
    
    char operacion; // Declarar la variable de tipo caracter "operacion"
    
    scanf("%c",&operacion); // Determinar el simbolo introducido a la variable "operacion"
    getchar();
    
    printf("\nDame otro numero: "); // Pedir al usuario un segundo numero
    
    float num2; // Declarar la variable decimal "num2"
    
    scanf("%f",&num2); // Determinar el valor introducido a la variable "num2"
    getchar();
    
    // Dependiendo de que operacion haya elegido el usuario, el programa sumara, restara, multiplicara o dividira los dos valores introducidos y mostrara el resultado en la pantalla
    if(operacion == '+')
    {
        printf("\n%.3f + %.3f = %.3f", num1, num2, num1 += num2);
    }
    
    if(operacion == '-')
    {
        printf("\n%.3f - %.3f = %.3f", num1, num2, num1 -= num2);
    }
    
    if(operacion == '*')
    {
        printf("\n%.3f * %.3f = %.3f", num1, num2, num1 *= num2);
    }
    
    if(operacion == '/')
    {
        printf("\n%.3f / %.3f = %.3f", num1, num2, num1 /= num2);
    }
        
    getchar();
    return (0); // Devolver 0. (Final del programa)
}