//18. Write a function to check if an integer is negative. Function declaration should be: int IsNegative(int num);

#include <stdio.h> // Introducir libreria "stdio.h"

int IsNegative(int num) // Declarar la variable entera "IsNefative" que nos mostrara si el numero que introduciremos en la variable "ent" es positivo o negativo
{
    printf("El numero %d es: ", num);
    if(num < 0)
    {
        printf("Negativo");
    }
    else
    {
        printf("Positivo");
    }
}


// Punto de entrada del programa
int main()
{
    int ent; // Declarar la variable entera "ent"
    int calcularsigno; // Declara la variable entera "calcularsigno"
    
    printf("Introduce un numero entero: "); // Pedir un numero al usuario
    
    scanf("%d",&ent); // Determinar valor introducido a "ent"
    getchar();
    
    calcularsigno = IsNegative(ent); // Determinar la variable "IsNefative" a la variable "calcularsigno"
        
    getchar();
    return (0); // Devolver 0. (Final del programa)
}