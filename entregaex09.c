//09. Write a program that asks for seconds and convert it into minutes, hours and days

#include <stdio.h> // Importar libreria "stdio.h"

// Punto de entrada del programa
int main()
{
    printf("Dame una cifra en segundos: "); // Mostrar en la pantalla lo que esta entre " "

    int segundos; // Declarar una variable de tipo entera nombrada "segundos"
    
    scanf("%d",&segundos); // Determinar el valor introduciodo a la variable "segundos"
    getchar();
    
    while ( segundos <= 0 ){ // Si se introduce un valor negativo o igual a 0, pedir que introduzca otro valor hasta que este sea positivo y determinarlo en variable "segundos"
        printf("Dame una cifra mayor que zero por favor: ");
        scanf("%d",&segundos);
        getchar();
    }
    
    int seg1 = segundos; // Declarar dos variables enteras "seg1" y "seg2" y determinarles el mismo valor que el de la variable "segundos"
    int seg2 = segundos;
    
    int minutostotal; // Declarar una variable entera "minutostotal" y determirale la operacion "seg1 /= 60"
    minutostotal = seg1 /= 60;
    
    int mintot1 = minutostotal; // Declarar quatro variables enteras "mintot1, 2, 3 y 4" y determinarles el mismo valor que la variable "minutostotal"
    int mintot2 = minutostotal;
    int mintot3 = minutostotal;
    int mintot4 = minutostotal;
    
    int dias; // Declarar una variable entera "dias" y determirale la operacion "mintot1 /= 1440"
    dias = mintot1 /= 1440; 
    
    int dias1 = dias; // Declarar dos variables enteras "dias1" "dias2" y determinarles el mismo valor que "dias"
    int dias2 = dias;
    
    int horas; // Declarar una variable entera "horas" y determirale la operacion "(mintot2 /= 60) - (dias1 *= 24)"
    horas = (mintot2 /= 60) - (dias1 *= 24);
    
    int horas1 = horas; // Declarar una variable entera "horas1" y determinarle el mismo valor que "horas"
    
    int minutosrest; // Declarar una variable entera "minutosrest" y determirale la operacion "mintot3 - (dias2 *= 1440) - (horas1 *= 60)"
    minutosrest = mintot3 - (dias2 *= 1440) - (horas1 *= 60);
    
    int segundosrest; // Declarar una variable entera "segundosrest" y determirale la operacion "seg2 - (mintot4 *= 60)"
    segundosrest = seg2 - (mintot4 *= 60);
    
    // Mostrar en la pantalla lo que esta escrito entr " " sustituiyendo los "%d" por las variables siguientes
    printf("\n%d segundos son:\n%d = dias\n%d = horas\n%d = minutos\n%d = segundos\n", segundos, dias, horas, minutosrest, segundosrest);
    
    getchar();
    return (0); // Devolver 0. (Final del programa)
} 
