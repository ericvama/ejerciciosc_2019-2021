//Pedir una secuencia de números positivos y mostrar la suma de dichos números.
	//NOTA: Cuando se introduce un número negativo, el programa deja de pedir números y finaliza.
#include <stdio.h>
#include <stdlib.h>
 
int main () {
 
   int tmp;
   int sumatotal=0;
   /*
   do{
        printf("Dame 1 numero\n");
        scanf("%d",&tmp);
        getchar();
        if(tmp>=0){
            sumatotal+=tmp;
        }
       
   }while(tmp>=0);
   */
    printf("Dame 1 numero\n");
    scanf("%d",&tmp);
    getchar();
   while(tmp>=0){
       sumatotal+=tmp;
       printf("Dame 1 numero\n");
       scanf("%d",&tmp);
       getchar();
   }
 
   printf("Sumatotal = %d\n",sumatotal);
 
   getchar();
   return(0);
}