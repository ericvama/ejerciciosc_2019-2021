//06. Write a program that asks for a value in centimeter and convert it into meter and kilometer.

#include <stdio.h> // Importar libreria "stdio.h"

// Punto de entrada del programa
int main()
{
    printf("Escribe una distancia en centimetros: "); // Mostrar en la pantalla lo que esta escrito entre " "
  
    float distans; // Declarar una variable de tipo decimal nombrada "distans"
    
    scanf("%f",&distans); // Determinar el valor introduciodo a la variable "distans"
    getchar(); // Espacio para continuar
    
    float distans2 = distans; // Declarar dos variable de tipo decimal "distans2" y "distans3" y determinarles el mismo valor que el de la variable "distans"
    float distans3 = distans;
    
    printf("\n%f centimetros son %f metros\n", distans, distans2 /= 100); // Mostrar el texto entre " ", sustituyendo "%d" por la variable "distans" y "%f" por la operacion siguiente
    printf("%f centimetros son %9.7f kilometros", distans, distans3 /= 100000);
    
    getchar();
    return 0; // Devolver 0. (Final del programa)
}