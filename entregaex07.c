//07. Write a program that asks for a temperature in Celsius(°C) and convert it into Fahrenheit(°F).

#include <stdio.h> // Importar libreria "stdio.h"

// Punto de entrada del programa
int main()
{
    printf("Escribe un grado de temperatura en Celsius: "); // Mostrar en la pantalla lo que esta escrito entre " "

    int TempCel; // Declarar una variable de tipo entera nombrada "TempCel"
    
    scanf("%d",&TempCel); // Determinar el valor introduciodo a la variable "TempCel"
    getchar();
    
    float TempFah = (TempCel *= 1.8) + 32; // Declarar una variable de tipo decimal nombrada "TempFah" y determinarle la operacion siguiente

    printf("\n%d grados Cesius son %.1f grados Fahrenheit", TempCel, TempFah); // Mostrar en la pantalla lo que esta escrito entre " ", sustituyendo "%d" y "%f" por las dos variables 
       
    getchar();
    return (0); // Devolver 0. (Final del programa)
}